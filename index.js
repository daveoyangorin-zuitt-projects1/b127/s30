const express = require('express');
const mongoose = require('mongoose')
const app = express()
const port = 3002;

mongoose.connect("mongodb+srv://admin:admin@zuitt-bootcamp.ir0ca.mongodb.net/batch127_to-do?retryWrites=true&w=majority",
	{
		useNewUrlParser:true,
		useUnifiedTopology:true
	});


let db = mongoose.connection;

db.on("error",console.error.bind(console,"connection error"));
db.once("open",()=> console.log("were connected to the cloud database"))

const taskSchema = new mongoose.Schema({
	user: String,
		status:{
			type:String,
			default:"pending"
		}
})

const Task = mongoose.model("task" , taskSchema)

app.use(express.json());
app.use(express.urlencoded({ extended:true }))

app.post('/signup' ,(req, res) =>{
	Task.findOne({ user: req.body.user}, (err,result) => {
		if(result !== null && result.user == req.body.user){
			return res.send("duplicate task found")
		}else{
			let newTask = new Task({
				user: req.body.user
			})
			newTask.save((saveErr, savedTask) =>{
				if(saveErr){
					return console.error(saveErr)
				}else{
					return res.status(201).send("new task created")
				}
			})
		}
	})
})


app.get('/users',(req,res)=>{
	Task.find({}, (err, result)=>{
		if(err){
			return console.log(err)
		}else{
			return res.status(200).json({
				data: result
			})
		}
	})

})


app.listen(port, () => console.log(`Server is running at port ${port}`))
